Ruby version: 2.4.1

Rails version: rails, 5.1.7

SQLITE

How to run this project

1) clone project (giT clone https://pranay05@bitbucket.org/pranay05/tms.git)

2) bundle install for fetching all gem libraries

4) rails s

5) rake db:seed

6) To test the APIs download and export the test postman collection present at path '<project_path>/TMS.postman_collection'

Summary

APIs Contracts

1) Signup API

baser_url = localhost:3000/api/v1/

HTTP method: POST HTTP headers: {'Content-Type': 'application/json'} Endpoint: {base_url}/users Request payload: { "user":
                                                                                                                 	{	"fullname": "john doe",
                                                                                                                 		"email": "john@gmail.com",
                                                                                                                 		"username": "johnwick",
                                                                                                                 		"password": "password",
                                                                                                                 		"organisation_id": 1

                                                                                                                 	}

                                                                                                                 }

2) Login

HTTP metod: GET
HTTP headers: { 'Content-Type': 'application/json'}
Endpoint: {base_url}/sessions/create
Request payload: {
                 	"email": "test@gmail.com",
                 	"password": "password1"
                 }

3) forgot password

HTTP method: POST
HTTP headers: { 'Content-Type': 'application/json'}
Endpoint: {base_url}/password/forgot
Request payload: {
                 	"email": "abc@gmail.com"
                 }

4) Reset password

HTTP method: POST
HTTP headers: { 'Content-Type': 'application/json'}
Endpoint: {base_url}/password/reset
Request payload: {
                 	"email": "abc@gmail.com",
                 	"token": "bb32682dc261e29fa260",
                 	"password": "password2"
                 }

5) Post tweet

HTTP method: POST
HTTP headers: {'Authorization': TOKEN token='token fromm login', 'Content-Type': 'application/json'}
Endpoint: {base_url}/post/create
request_payload:  {"post":
                  	{
                  		"title": "my second post dfddfd",
                  	    "url": "www.linkhere.com"

                  	}
                  }
6) List TWEET

HTTP method: GET
HTTP headers: {'Authorization': TOKEN token='token fromm login', 'Content-Type': 'application/json'}
Endpoint: {base_url}/posts/list
REQUEST_PAYLOAD:

7) Delete tweet:
HTTP headers: {'Authorization': TOKEN token='token fromm login', 'Content-Type': 'application/json'}
Endpoint: {base_url}/post/destroy

request_payload:  {
                  	"id": 5
                  }


8) Update Tweet

HTTP method: PATCH
HTTP headers: {Authorization': TOKEN token='token from login', 'Content-Type': 'application/json'}
Endpoint: {base_url}/post/update

request_payload: {
                 	"post":
                 	{
                 		"title": "updte me 3 fdf",
                 		"id": 2
                 	}

                 }



