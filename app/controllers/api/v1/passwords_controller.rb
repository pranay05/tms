class Api::V1::PasswordsController < ApplicationController

  def forgot
    msg, code = PasswordService.generate_reset_link(params)
    render json: msg,status: code

  rescue Exception => e

    render json: { error_msg: 'Opps! something went wrong', error: e.message}, status: 500
  end

  def reset
    msg, code = PasswordService.reset_password(params)
    render json: msg,status: code
  rescue Exception => e

    render json: {error_msg: 'Opps! something went wrong', error: e.message}, status: 500
  end
end
