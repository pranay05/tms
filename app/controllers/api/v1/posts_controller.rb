class Api::V1::PostsController < ApplicationController
  before_action :authorize_token
  before_action :set_user_id, only: [:create]
  before_action :set_post, only: [:update, :destroy]
  before_action :authorize_action, only: [:update, :destroy]

  def index
    @posts = Post.joins(:user).where("users.organisation_id=?",@current_user.organisation_id)
    render json: @posts, status: 200

  rescue Exception => e

    render json: { error_msg: 'Opps! something went wrong', error: e.message}, status: 500
  end

  def create
    msg, code = PostService.create_post(post_params)
    render json: msg,status: code

  rescue Exception => e

    render json: { error_msg: 'Opps! something went wrong', error: e.message}, status: 500
  end

  def update
    msg, code = PostService.update_post(@post,post_params)
    render json: msg,status: code

  rescue Exception => e

    render json: { error_msg: 'Opps! something went wrong', error: e.message}, status: 500

  end

  def destroy
    msg, code = PostService.destroy_post(@post)

    render json: msg,status: code

  rescue Exception => e

    render json: { error_msg: 'Opps! something went wrong', error: e.message}, status: 500
  end

  private

  def post_params
    params.require(:post).permit(:title, :url, :user_id )
  end

  def set_post
    @post = Post.find_by(id: params[:post][:id])
    raise Exception, 'Post Not Found' if @post.nil?
  end

  def set_user_id
    params[:post][:user_id] = @current_user.id
  end

  def authorize_action
    is_authorized = (@current_user&.roles&.pluck(:title)&.include? 'Admin') || (@post.user.id ==  @current_user.id)
    render json: { error: 'Permission Denied.' }, status: :forbidden if !is_authorized
  end

end
