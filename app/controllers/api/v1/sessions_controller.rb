class Api::V1::SessionsController < ApplicationController

  def create
    user = User.find_by_email(params[:email])
    # If the user exists AND the password entered is correct.
    if user && user.authenticate(params[:password])
      @api_key = ApiKey.new(access_token: generate_key, expiry_time: expiry_time, user_id: user.id)
      @api_key.save!
      render json: { access_token: @api_key.access_token, message: 'success full log in', user: user }, status: 200
    else
      render json: {  message: 'wrong username/password' }, status: 403
    end

  rescue Exception => e

    render json: { error_msg: 'Opps! something went wrong', error: e.message}, status: 500
  end

  def destroy
    @api_key = ApiKey.find_by(access_token: params[:access_token])
    if @api_key.destroy
      render json: {  message: 'Logged out' }, status: 200
    else
      render json: {  message: 'could not log out' }, status: 403
    end

  rescue Exception => e

    render json: { error_msg: 'Opps! something went wrong', error: e.message}, status: 500
  end

  private

  def generate_key
    SecureRandom.hex
  end

  def expiry_time
    DateTime.now + 2
  end
end
