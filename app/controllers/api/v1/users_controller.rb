class Api::V1::UsersController < ApplicationController
  before_action :authorize_token, only: [:update, :destroy,:set_user]
  before_action :authorize_action, only: [:update, :destroy]
  before_action :set_user, only: [:update, :destroy]

  def index
  end

  def create
    msg, code = UserService.create_user(user_params)
    render json: msg,status: code

  rescue Exception => e

    render json: { error_msg: 'Opps! something went wrong', error: e.message}, status: 500
  end

  def update

    msg, code = UserService.update_user(@user,user_params)
    render json: msg,status: code

  rescue Exception => e

    render json: { error_msg: 'Opps! something went wrong', error: e.message}, status: 500
  end

  def destroy

    msg, code = UserService.destroy_user(@user)
    render json: msg,status: code

  rescue Exception => e

    render json: { error_msg: 'Opps! something went wrong', error: e.message}, status: 500
  end

  private

  def user_params
    params.require(:user).permit(:fullname, :password, :password_confirmation, :email, :username ,:organisation_id,
                                 :phonenumber)
  end

  def set_user
    @user = params[:id].present? ? User.find(params[:id]) : @current_user.id
  end

end
