class ApplicationController < ActionController::API
  include ActionController::HttpAuthentication::Token::ControllerMethods


  def authorize_token
    authenticate_or_request_with_http_token do |token, _options|
      @api_key = ApiKey.where(access_token: token).where('api_keys.expiry_time > ?', DateTime.now)[0]
      if !@api_key.nil?
        @current_user = User.find_by(id: @api_key.user_id, is_active: true)
      else
        render json: { error: 'HTTP Token: Access denied.' }, status: :forbidden
      end
    end
  end


end
