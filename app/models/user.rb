class User < ApplicationRecord
  has_secure_password
  has_many :user_roles, dependent: :destroy
  has_many :roles, through: :user_roles
  has_many :posts, dependent: :destroy
  belongs_to :organisation

  # validates :email,
  #           uniqueness: { case_sensitive: false },
  #           presence: true,
  #           allow_blank: false

  validates_confirmation_of :password
  validates_presence_of :username, :fullname, :email, :organisation_id
  validates :password, :presence => {:message => "validation failed : Password field can not be left blank."},  on: :create
  validates_format_of :email, with: /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i
  # validates_uniqueness_of :email


  def generate_password_token!
    self.reset_password_token = generate_token
    self.reset_password_sent_at = Time.now.utc
    save!
  end

  def password_token_valid?
    (self.reset_password_sent_at + 4.hours) > Time.now.utc
  end

  def reset_password!(password)
    self.reset_password_token = nil
    self.password = password
    save!
  end

  private

  def generate_token
    SecureRandom.hex(10)
  end
end
