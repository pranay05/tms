class PasswordService

  def self.generate_reset_link(params)
    if params[:email].blank?
      return render json: {error: 'Email not present'}
    end

    user = User.find_by(email: params[:email].downcase)

    if user.present?
      user.generate_password_token!
      # SEND EMAIL HERE BUT WE WILL SEND IN RESPONSE
      return {status: 'ok', email_response: {token: user.reset_password_token, email: user.email} }, 200
    else
      return {error: ['Email address not found. Please check and try again.']}, 404
    end
  end

  def self.reset_password(params)
    token = params[:token].to_s

    if params[:email].blank?
      return render json: {error: 'Token not present'}
    end

    user = User.find_by(reset_password_token: token)

    if user.present? && user.password_token_valid?
      if user.reset_password!(params[:password])
        return {status: 'ok'}, 200
      else
        return {error: user.errors.full_messages}, 422
      end
    else
      return {error:  ['Link not valid or expired. Try generating a new link.']}, 404
    end
  end


  private

end
