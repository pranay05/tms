class PostService

  def self.create_post(post_params)
    post = Post.new(post_params)
    if post.save!
      msg = { message: 'Post has been succesfully created' }
      return msg, 200
    else
      msg = { error: 'unable to create post'  }
      return msg, 400
    end
  end

  def self.update_post(post, post_params)
    #  todo check for role for upatig post
    if post.update(post_params)
      msg = { message: 'succesfully updated' }
      return msg, 200
    else
      msg = { error: 'unable to update'  }
      return  msg, 400
    end
  end

  def self.destroy_post(post)
    if post.destroy!
      msg = { message: 'successfully deleted Post' }
      return msg, 200
    else
      msg = { error: 'unable to delete'  }
      return  msg, 400
    end

  end

end
