class UserService

  def self.create_user(user_params)
    user = User.new(user_params)
    if user.save
      # 2 is 'Non-admin' default role
      user.user_roles.build(role_id: 2).save!
      msg = { message: 'succesfully signed up' }
      return msg, 200
    else
      msg = { error: 'unable to save data'  }
      return  msg, 400
    end
  end

  def self.update_user(user, user_params)
    #  todo check for role for upatig user
    if user.update(user_params)
      msg = { message: 'succesfully updated' }
      return msg, 200
    else
      msg = { error: 'unable to update'  }
      return  msg, 400
    end
  end

  def self.destroy_user(user)
    if user.update_attributes(is_active: false)
      msg = { message: 'successfully soft deleted' }
      return msg, 200
    else
      msg = { error: 'unable to soft delete'  }
      return msg, 400
    end
  end

end
