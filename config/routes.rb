Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  namespace :api, defaults: { format: 'json' } do
    namespace :v1 do
      resources :users do
      end

      post 'post/create', to: 'posts#create'
      get 'posts/list', to: 'posts#index'
      patch 'post/update', to: 'posts#update'
      post 'post/destroy', to: 'posts#destroy'
      get 'sessions/create', to: 'sessions#create'
      post 'sessions/logout', to: 'sessions#destroy'
      post 'password/forgot', to: 'passwords#forgot'
      post 'password/reset', to: 'passwords#reset'
    end
  end
end
