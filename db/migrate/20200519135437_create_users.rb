class CreateUsers < ActiveRecord::Migration[5.1]
  def change
    create_table :users do |t|
      t.string :fullname
      t.string :password_digest,  null: false, default: ""
      t.string   :reset_password_token
      t.datetime :reset_password_sent_at
      t.string :mobilenumber
      t.string :email,  null: false, default: ""
      t.string :salt
      t.boolean :is_active,  null: false, default: true
      t.string :username,  null: false, default: ""
      t.integer :organisation_id,  null: false

      t.timestamps
    end
  end
end
