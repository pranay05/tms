class CreateApiKeys < ActiveRecord::Migration[5.1]
  def change
    create_table :api_keys do |t|
      t.string :access_token,  null: false
      t.datetime :expiry_time,  null: false
      t.integer :user_id,  null: false

      t.timestamps
    end
  end
end
