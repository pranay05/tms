require 'rails_helper'

# Test suite for the Post model
RSpec.describe Post, type: :model do
  # Association test
  # ensure an item record belongs to a single user record
  it { should belong_to(:user) }
  # Validation test
  # ensure column tile is present before saving
  it { should validate_presence_of(:title) }
  it { should validate_presence_of(:user_id) }
end