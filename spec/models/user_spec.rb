require 'rails_helper'

# Test suite for the User model
RSpec.describe User, type: :model do
  # Association test
  # ensure user model has a 1:m relationship with the post model
  it { is_expected.to have_many(:posts).dependent(:destroy) }
  it { should have_many(:user_roles).dependent(:destroy) }
  # Validation tests
  it { should validate_presence_of(:username) }
  it { should validate_presence_of(:fullname) }
  it { should validate_presence_of(:password) }
  it { should validate_presence_of(:email) }
  it { is_expected.to validate_presence_of(:organisation_id) }
end